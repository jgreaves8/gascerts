<?php

use Illuminate\Database\Seeder;

class PropertyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('en_GB');

        for($i=0; $i<=3; $i++):
            $property = new \App\Property();
            $property->line1 = $faker->buildingNumber . ' ' . $faker->streetName;
            $property->slug = str_replace(' ', '-', strtolower($property->line1));
            $property->line2 = $faker->streetName;
            $property->postcode = $faker->postcode;
            $property->save();
        endfor;
    }
}
