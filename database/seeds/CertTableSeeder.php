<?php

use Illuminate\Database\Seeder;

class CertTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('en_GB');

        foreach(\App\Property::all() as $property){
            $rand = rand(0, 400);
            if($rand <= 100){
                $this->addCert($property->id, 'gas');
            }
            if($rand > 100 && $rand <= 200){
                $this->addCert($property->id, 'electric');
            }
            if($rand > 200 && $rand <=300){
                $this->addCert($property->id, 'gas');
                $this->addCert($property->id, 'electric');
            }
        }
    }

    public function addCert($property_id, $type){
        $faker = \Faker\Factory::create('en_GB');
        $cert = new \App\Cert();
        $cert->property_id = $property_id;
        $cert->type = $type;
        $diff = '-1 year';
        if($type == 'electric'){
            $diff = '-4 years';
        }
        $dt = $faker->dateTimeBetween($startDate = $diff, $endDate = 'now');
        $cert->date = $dt->format("Y-m-d");
        $cert->save();
    }
}
