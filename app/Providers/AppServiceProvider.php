<?php

namespace App\Providers;

use App\Observers\PropertyObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::share('validProperties', \App\Property::valid());
        View::share('propertiesNeedingGas', \App\Property::needsGas());
        View::share('propertiesNeedingElectric', \App\Property::needsElectric());
    }
}
