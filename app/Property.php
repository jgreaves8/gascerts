<?php

namespace App;

use Carbon\Carbon;
use App\Cert;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Property
 *
 * @property int $id
 * @property string $slug
 * @property string $line1
 * @property string $line2
 * @property string $postcode
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Cert[] $cert
 * @property-read int|null $cert_count
 * @property-read mixed $electric_due_date
 * @property-read mixed $full_address
 * @property-read mixed $gas_due_date
 * @property-read mixed $needs_class
 * @method static \Illuminate\Database\Eloquent\Builder|Property newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Property newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Property query()
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereLine1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereLine2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property wherePostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Property whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Property extends Model
{
    protected $appends = ['fullAddress', 'needsClass', 'hasValidGas', 'hasValidElectric', 'gasExpires', 'electricExpires', 'checker'];

    protected $fillable = ['line1', 'line2', 'postcode', 'checker_id'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function checker(){
        return $this->belongsTo(\App\Checker::class);
    }

    public function getCheckerAttribute(){
        if($this->checker()->exists()){
            return $this->checker()->first()->name;
        }
        return '';
    }

    public function getGasExpiresAttribute(){
        if($this->hasValidGas){
            $cert = $this->certs()->whereType('gas')->orderBy('expiryDate', 'desc')->first();
            return 'good for ' . Carbon::createFromFormat('Y-m-d', $cert->expiryDate)->longAbsoluteDiffForHumans(Carbon::now());
        }
        return '';
    }

    public function getElectricExpiresAttribute(){
        if($this->hasValidElectric){
            $cert = $this->certs()->whereType('electric')->orderBy('expiryDate', 'desc')->first();
            return 'good for ' . Carbon::createFromFormat('Y-m-d', $cert->expiryDate)->longAbsoluteDiffForHumans(Carbon::now());
        }
        return '';
    }

    public function getHasValidGasAttribute(){
        if(!$latestGas = $this->latestCert('gas')){
            return false;
        }
        return $latestGas->isValid();
    }

    public function getHasValidElectricAttribute(){
        if(!$latestGas = $this->latestCert('electric')){
            return false;
        }
        return $latestGas->isValid();
    }

    public function latestCert($type = 'gas'){
        return $this->certs()->whereType($type)->orderBy('expiryDate', 'desc')->first() ?? false;
    }

    public function upToDate(){
        return $this->hasValidGas && $this->hasValidElectric;
    }

    protected static function booted()
    {
        static::creating(function ($property) {
            $property->slug = str_replace(' ', '-', strtolower($property->line1));
        });
    }

    public function getFullAddressAttribute()
    {
        return $this->line1 . ', ' . $this->line2 . ', ' . $this->postcode;
    }

    public function hasType($type)
    {
        return ($this->certs()->where('type', $type)->exists());
    }

    public function getNeedsClassAttribute()
    {
        $has_gas = $has_electric = $has_both = false;
        if($this->hasValidGas){
            $has_gas = true;
        }
        if ($this->hasValidElectric) {
            $has_electric = true;
        }
        if ($has_gas && $has_electric) {
            return 'has-both';
        }
        if ($has_gas) {
            return 'has-gas needs-electric';
        }
        if ($has_electric) {
            return 'has-electric needs-gas';
        }
        return 'needs-both needs-gas needs-electric';
    }

    public function certs()
    {
        return $this->hasMany(Cert::class)->orderBy('expiryDate', 'asc');
    }

    public static function needsGas(){
        $properties = Property::all();
        return $properties->filter(function($property){
            return !$property->hasValidGas;
        });
    }

    public static function searchResults(){
        return Property::where('line1', 'LIKE', '%'.$_GET['s'].'%')
            ->orWhere('line2', 'LIKE', '%'.$_GET['s'].'%')
            ->orWhere('postcode', 'LIKE', '%'.$_GET['s'].'%')
            ->orderBy('line1', 'desc')->get();
    }

    public static function needsElectric(){
        $properties = Property::orderBy('line1', 'desc')->get();
        return $properties->filter(function($property){
            return !$property->hasValidElectric;
        });
    }

    public static function valid(){
        $properties = Property::orderBy('line1', 'desc')->get();
        return $properties->filter(function($property){
            return $property->hasValidElectric && $property->hasValidGas;
        });
    }

}
