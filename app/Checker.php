<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checker extends Model
{
    protected $fillable = ['name', 'phone', 'email'];
}
