<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Cert
 *
 * @property int $id
 * @property int $property_id
 * @property string $type
 * @property string $date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cert newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cert newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cert query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cert whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cert whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cert whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cert wherePropertyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cert whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cert whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Cert extends Model
{
    protected $fillable = ['type', 'date'];

    private $gasExpiry = 1;
    private $electricExpiry = 1;

    public function niceDate($field = 'date')
    {
        return Carbon::createFromFormat('Y-m-d', $this->$field)
            ->format('jS F Y');
    }

    public function carbon($field = 'date')
    {
        return Carbon::createFromFormat('Y-m-d', $this->{$field});
    }

    public function years()
    {
        return $this->type == 'gas' ? $this->gasExpiry : $this->electricExpiry;
    }

    public function expires()
    {
        return $this->carbon()
            ->addYears($this->years())
            ->format('jS F Y');
    }

    public function addExpiry(){
        $monthsToAdd = $this->type == 'gas' ? 11 : 59;
        $this->expiryDate = $this->carbon()->addMonths($monthsToAdd);
    }

    public function expiresIn(){
        return $this->carbon('expiryDate')->diffForHumans(Carbon::now());
    }

    public function isValid(){
        return $this->carbon('expiryDate')->greaterThan(Carbon::now());
    }

    public function validityBadge(){
        return $this->isValid()
            ? '<span style="font-size: 100%;" class="badge badge-success">Valid</span>'
            : '<span style="font-size: 100%;" class="badge badge-danger">Expired</span>';
    }

    public function urlToFile(){
        return '/file/' . $this->id;
    }

}
