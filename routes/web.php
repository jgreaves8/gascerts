<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/properties/{property}', 'PropertyController@show')->name('showProperty');
Route::delete('/properties/{property}', 'PropertyController@destroy')->name('deleteProperty');

Route::get('/properties.json', function () {
    return \App\Property::all();
});

Route::get('/file/{file}', function(\App\Cert $file){
    return Storage::download($file->file);
});

//Route::get('/checkers', 'CheckCon')
Route::get('mail', function(){
    Mail::to('jgreaves8@gmail.com')
        ->send(new \App\Mail\CertExpiring());
});

Route::get('/', 'PropertyController@index');
Route::post('/', 'PropertyController@store');
Route::post('/properties/{property}/cert', 'CertController@store')->name('addCert');
Route::delete('/{cert}', 'CertController@destroy')->name('deleteCert');
Route::get('/checkers', 'CheckerController@index');
Route::post('/checkers', 'CheckerController@store')->name('addChecker');
Route::delete('/checkers/{checker}', 'CheckerController@destroy');
Route::get('/api-checkers', function(){
    return \App\Checker::all();
});

Route::get('/slug-exists/{slug}', function($slug){
    return \App\Property::whereSlug($slug)->exists();
});

//Route::get('/pdf', function () {
//    mail('jgreaves8@gmail.com', 'te1', 't1sd');
//    return Mail::to('jgreaves8@gmail.com')->send(new \App\Mail\OrderShipped());

//    return Storage::disk('sftp')->files('laravel-files');

//    return $exists = Storage::disk('sftp')->get('laravel-files/IpnrEmeHFLKjsvuQ4FlLJ1bIVExFStb9FwnOrKt4.pdf');

    // mail('jgreaves8@gmail.com', 'test', 'test');
    // Storage::disk('sftp')->put('file.txt', 'Contents');
//    return view('pdf');
//});

Route::post('/aaa', function (Illuminate\Http\Request $request) {

    $request->file('pdf')->store('electric');
    $request->file('pdf')->store('gas');

//    $path = $request->file('pdf')->store(
//        'laravel-files', 'sftp'
//    );

    // $request->pdf->storeAs('images', 'filename.pdf');
});

//@todo https://plugins.jetbrains.com/plugin/13892-codeyzer
//@todo main
/*
 * add and store cert, [id, date, file]
 * check once per day [update gasUpToDate, electricUpToDate]
 * add expiry date when creating cert?
 *
 *
 * new
 *      order by expiring soonest?
 *      link straight to file on homepage?
 *      add texting
 *      sort out reminder crons
 *      sort out google calendar
 */
