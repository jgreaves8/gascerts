@php
    /* @var \App\Property $property */
    /* @var \App\Cert $cert */
@endphp

@extends('master')

@section('content')

    <div id="vue" style="margin-top:3rem;">

        <div class="row">
            <div class="col">
                <form method="POST" action="{{route('deleteProperty', $property)}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE"/>
                    <button class="mb-3 btn btn-sm btn-danger">Delete Property</button>
                </form>
            </div>
        </div>

        @if($property->upToDate())
            <div class="row">
                <div class="col">
                    <div class="alert alert-success">
                        <h3>Has valid Gas and Electric</h3>
                    </div>
                </div>
            </div>

        @else
            <div class="row">
                <div class="col">
                    <div class="alert alert-danger">
                        @if(!$property->hasValidGas)
                            <h3 class="mb-0">Needs a valid gas certificate</h3>
                        @endif
                        @if(!$property->hasValidElectric)
                            <h3 class="mb-0">Needs a valid electric certificate</h3>
                        @endif
                    </div>
                </div>
            </div>
        @endif

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif

        @if(Session::has('error'))
            <div class="alert alert-danger">
                {{ Session::get('danger') }}
            </div>
        @endif

        <div class="row">
            <div class="col">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Issued</th>
                            <th>Expires</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($property->certs as $cert)
                        <tr>
                            <td>{{ucwords($cert->type)}}</td>
                            <td>{{$cert->niceDate()}}</td>
                            <td>
                                {{$cert->niceDate('expiryDate')}}
                                {!! $cert->validityBadge() !!}
                                <a href="/file/{{$cert->id}}" class="btn btn-sm btn-info">File</a>
                            </td>
                            <td>
                                <form method="POST" action="{{route('deleteCert', $cert)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <input type="submit" class="btn btn-sm btn-danger" value="Delete"/>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col col-md-6 offset-md-3">
                <h2>Add Certificate</h2>

                <form method="POST" action="{{route('addCert', $property)}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Type</label>
                        <div class="col-sm-9">
                            <select name="type" v-model="certType" class="form-control">
                                <option value="gas">Gas</option>
                                <option value="electric">Electric</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Valid From</label>
                        <div class="col-sm-9">
                            <input class="form-control" v-model="ourDate" type="date" name="date"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">File</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="file" name="file"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Save">
                    </div>
                </form>
                <div class="alert alert-success" v-html="reminderSentWhen">
                </div>
            </div>
        </div>

        @php
            //@todo does it have a valid X cert?
            //@todo if not, allow upload
        @endphp

    </div>
@stop

@section('scripts')
    <script>

        new Vue({
            el: '#vue',
            data() {
                return {
                    certType: 'gas',
                    ourDate: '{{date("Y-m-j")}}',
                    needsCerts: {{!$property->upToDate() ? 1 : 0}},
                    gasReminder: {
                        inMonths: 11,
                        text: '11 months'
                    },
                    electricReminder: {
                        inMonths: 59,
                        text: '4 years 11 months'
                    }
                }
            },
            computed: {
                unixDate() {
                    return this.ourDate.split('-').join('')
                },
                reminderSentWhen() {
                    let {inMonths, text} = this.certType === 'gas' ? this.gasReminder : this.electricReminder;
                    let onDate = moment(this.unixDate).add(inMonths, 'months').calendar()
                    return `Reminder will be sent in <strong>${text}</strong> on the <strong>${onDate}</strong>`;
                }
            }
        })

    </script>

@stop
