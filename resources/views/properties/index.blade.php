@extends('master')

@section('content')
    <div class="properties-index mt-5" id="vue">

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif

        @if(Session::has('error'))
            <div class="alert alert-danger">
                {{ Session::get('danger') }}
            </div>
        @endif

        <property-buttons @property-added="propertyAdded" v-bind:showing="showing" @update-showing="updateShowing"></property-buttons>

            <div class="row">

                <div class="col">

                <table class="table table-striped">
                    <thead>
                        <th class="text-center">Property</th>
                        <th class="text-center">Gas</th>
                        <th class="text-center">Electric</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <tr v-for="prop in properties"
                            is="property"
                            v-bind:showing=showing
                            v-bind:prop=prop
                            :key=prop.id>
                        </tr>
                    </tbody>
                </table>

                </div>
            </div>


    </div>
@stop

@section('scripts')
    <script>
        new Vue({
            el: '#vue',
            data() {
                return {
                    properties: {!!$properties!!},
                    showing: 'all'
                }
            },
            methods: {
                propertyAdded(property){
                    this.properties.push(property);
                },
                updateShowing(newValue) {
                    this.showing = newValue;
                },
            }

        })
    </script>
@stop
