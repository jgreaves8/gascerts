@extends('master')

@section('content')
    <div id="vue" style="margin-top:3rem;">

        <div class="row">
            <div class="col-md-7">
                <div v-if="!checkers.length" class="alert alert-danger">
                    <h3 class="mb-0">Add some Checkers using the form on the right</h3>
                </div>
                <table v-if="checkers.length" class="table table-striped">
                    <thead>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <tr v-for="(checker, index) in checkers">
                            <td v-text="checker.name"></td>
                            <td v-text="checker.phone"></td>
                            <td v-text="checker.email"></td>
                            <td class="text-right">
                                <button @click="del($event, checker, index)" class="btn-danger btn btn-sm">Delete</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-5">
                <h2>Add new Checker</h2>
                <form @submit="addChecker">
                    <div class="form-group">
                        <input class="form-control" type="text" v-model="checker.name" placeholder="Name" required/>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" v-model="checker.phone" placeholder="Phone (optional)"/>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="email" v-model="checker.email" placeholder="Email" required/>
                    </div>
                    <div class="form-group">
                        <button class="btn-primary">Add</button>
                    </div>
                </form>
            </div>
        </div>


    </div>
@stop

@section('scripts')
    <script>
        new Vue({
            el: '#vue',
            data() {
                return {
                    checker: {
                        name: '',
                        phone: '',
                        email: ''
                    },
                    checkers: {!!$checkers!!},
                }
            },
            methods: {
                del(event, checker, index){
                    event.target.innerHTML = '<i class="fas fa-spinner fa-spin"></i>';
                    axios.delete('/checkers/' + checker.id).then(r => {
                        if(r.status === 200){
                            this.checkers.splice(index, 1);
                        }
                    });
                },
                addChecker(event){
                    if(this.checker.name && this.checker.email){
                        axios.post('/checkers', this.checker).then(r => {
                            this.checkers.push(r.data);
                        });
                        this.checker = {name: '', phone: '', email: ''};
                        event.preventDefault();
                    }
                }
            }
        })
    </script>
@stop

@section('styles')
    <style>
        .fa-trash {
            font-size: 20px;
        }

        .fa-trash:hover{
            cursor: pointer;
        }

        .text-right button {
            width: 70px;
        }
    </style>
@stop
