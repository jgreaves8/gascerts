@extends('master')

@section('content')
    <div id="vue" style="margin-top:3rem;">
        <div class="row">
            <form method="POST" action="/" enctype="multipart/form-data">
                {{csrf_field()}}
                <select name="cert_type" required>
                    <option value=""></option>
                    <option value="gas">Gas</option>
                    <option value="electric">Electric</option>
                </select>
                <input type="file" name="pdf"/>
                <input type="submit"/>
            </form>
        </div>
    </div>
@stop
