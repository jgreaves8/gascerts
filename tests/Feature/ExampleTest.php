<?php

namespace Tests\Feature;

use Tests\TestCase;

class ExampleTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {

        $this->artisan('db:wipe');
        $this->artisan('migrate');
        $this->artisan('db:seed');
//        $property = new Property();
//        $property->line1 = 'est';
//        $property->line2 = 'est';
//        $property->slug = 'es1t';
//        $property->postcode = 'est';
//        $property->save();
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    function testPropertiesExist()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }
}
